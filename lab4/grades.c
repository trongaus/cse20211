// grades.c
// Author: Taylor Rongaus
// 9/27/15
// This program reads grade values from a data file into an array and then computes their average and their standard deviation

#include <stdio.h>
#include <math.h>

int main() // begin function main
{
	int x=0,s=0, n=0;
	int i; 
	double avg;
	int a[50];
	
	scanf("%d",&a[x]);			// read in data & place in array a
	while (a[x] > 0) {
		x++;
		scanf("%d",&a[x]);
	}

	for (i=0; i<x; i++){			// display elements of array
		printf("%d ",a[i]);
	}
	printf("\n");

	for (i=0; i<x; i++) {			// calculate sum of array elements
		s = s + a[i];
	}
	avg = ((double)s)/ ((double)i);		// determine average grade value
	printf("Average is: %.2lf\n", avg);

	s = 0;
	double newAvg;				// define variables for new avg. and stdDev
	double stdDev;

	for (i=0;i<x;i++){			// subtract mean from each original value, square result
		a[i]=(a[i]-avg)*(a[i]-avg);
		s = s + a[i];	
	}
	newAvg = ((double)s)/((double)i);	// determine average of new values
	stdDev = sqrt(newAvg);			// sqrt to determine stdDev
	printf("Standard Deviation is: %.2lf",stdDev);

	printf("\n");
	return 0;


} // end function main
