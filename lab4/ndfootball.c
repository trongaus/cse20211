// ndfootball.c
// Author: Taylor Rongaus
// 9/27/15
// This program will set up arrays of data of the Notre Dame football team's wins and losses and provide the user with a set of questions on the data

#include <stdio.h>

int displaywins(int year,int wins[]);			// function prototypes
int displaylosses(int year,int losses[]);
void lossyears(int minlosses, int losses[],int losssize);
void winyears(int minwins, int wins[],int winsize);
int totalwins(int wins[],int winsize);
int totallosses(int losses[],int losssize);

int main() 						// begin function main
{
	int num, numwins,numlosses, year;
	int minlosses, minwins;
	int totWins, totLoss, winsize, losssize;
	int i=0;
	
	int wins[] = {6, 8, 6, 8, 5, 5, 6, 6, 8, 7, 4,
		6, 7, 7, 6, 7, 8, 6, 3, 9, 9, 10,
		8, 9, 10, 7, 9, 7, 5, 9, 10, 6, 6,
		3, 6, 7, 6, 6, 8, 7, 7, 8, 7, 9,
		8, 7, 8, 9, 9, 10, 4, 7, 7, 9, 9,
		8, 2, 7, 6, 5, 2, 5, 5, 2, 9, 7,
		9, 8, 7, 8, 10, 8, 8, 11, 10, 8, 9,
		11, 9, 7, 9, 5, 6, 7, 7, 5, 5, 8,
		12, 12, 9, 10, 10, 11, 6, 9, 8, 7, 9,
		5, 9, 5, 10, 5, 6, 9, 10, 3, 7, 6,
		8, 8, 12, 9, 8};
	int losses[] = {3, 1, 2, 0, 3, 4, 1, 0, 1, 0, 1,
		0, 0, 0, 2, 1, 1, 1, 1, 0, 0, 1,
		1, 1, 0, 2, 1, 1, 4, 0, 0, 2, 2,
		5, 3, 1, 2, 2, 1, 2, 2, 0, 2, 1,
		2, 2, 0, 0, 0, 0, 4, 2, 2, 0, 1,
		2, 8, 3, 4, 5, 8, 5, 5, 7, 1, 2,
		0, 2, 2, 2, 1, 2, 3, 0, 2, 3, 3,
		1, 3, 4, 2, 6, 4, 5, 5, 6, 6, 4,
		0, 1, 3, 3, 1, 1, 5, 3, 3, 6, 3,
		7, 3, 6, 3, 7, 6, 3, 3, 9, 6, 6,
		5, 5, 1, 4, 5};
	
	int endprogram = 0;
	int checkchoice = 0;
	int checkyear = 0;
	winsize = sizeof(wins)/sizeof(int);
	losssize = sizeof(losses)/sizeof(int);

	while (endprogram == 0) {			// prompt user with options for data display
		while (checkchoice == 0) {
	  		printf("\n1: display the record for a given year.\n2: display years with at least 'n' losses.\n3: find years with at least 'n' wins.\n4: display the total number of wins in ND football history.\n5: display the total number of losses in ND football history.\n6: exit\n");
	  		printf("Enter your choice: ");
	  		scanf("%d", &num);
	  		if (num == 1 || num == 2 || num == 3 || num == 4 || num == 5 || num == 6) {
	      			checkchoice = 1;
	    		}
	  		else {
	      			printf ("Error. Please enter a valid choice.\n");
	   		}
	  		printf ("\n");
		}

		switch (num) {				// switch for different possible user inputs
			case 1: 			// 1: record for a given year
				while (checkyear == 0) {
					printf("Enter the year: ");
					scanf("%d",&year);
					if ((year < 1900) || (year > 2014)) {
						printf("Error. Please enter a year between 1900 and 2014.\n");
					}
					else {
						checkyear = 1;
					}
				}
				numwins = displaywins(year,wins);
				numlosses = displaylosses(year,losses);
				printf("Wins: %d, Losses: %d\n",numwins,numlosses);
				checkchoice = 0;
				break;
			case 2:				// 2: min losses
				printf("Enter minimum number of losses: ");
				scanf("%d",&minlosses);
				lossyears(minlosses,losses,losssize);
				printf("\n");
				checkchoice = 0;
				break;
			case 3:				// 3: min wins
				printf("Enter minimum number of wins: ");
				scanf("%d",&minwins);
				winyears(minwins,wins,winsize);
				printf("\n");
				checkchoice = 0;
				break;
			case 4:				// 4: total wins
				totWins = totalwins(wins,winsize);
				printf("The total number of wins in ND football history is %d\n",totWins);
				checkchoice = 0;
				break;
			case 5:				// 5: total losses
				totLoss = totallosses(losses,losssize);
				printf("The total number of losses in ND football history is %d\n",totLoss);
				checkchoice = 0;
				break;
			case 6:				// 6: exit
				printf ("Good-bye!\n");
				endprogram = 1;
				break;
		}
	}
}							// end function main


int displaywins(int year, int wins[]) {			// begin function executions
	int num = year-1900;
	int numwins = wins[num];
	return numwins;
}

int displaylosses(int year, int losses[]) {
	int num = year-1900;
	int numlosses = losses[num];
	return numlosses;
}

void lossyears(int minlosses, int losses[],int losssize) {
	int i,year = 0;
	printf("Years with at least %d losses: ",minlosses);
	for(i=0;i<losssize;i++) {
		if (losses[i] >= minlosses) {
			year = 1900 + i;
			printf(" %d",year);
		}
	}
}

void winyears(int minwins, int wins[],int winsize) {
	int i,year = 0;
	printf("Years with at least %d wins: ",minwins);
	for(i=0;i<winsize;i++) {
		if (wins[i] >= minwins) {
			year = 1900 + i;
			printf(" %d",year);
		}
	}
}

int totalwins(int wins[], int winsize) {
	int sum = 0, i;
	for (i=0;i<=winsize;i++){
		sum = sum + wins[i];
	}
	return sum;
}

int totallosses(int losses[],int losssize) {
	int sum = 0, i;
	for (i=0;i<=losssize;i++){
		sum = sum + losses[i];
	}
	return sum;
}



