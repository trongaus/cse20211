// primes.c
// Author: Taylor Rongaus
// 9/27/15
// This program uses the sieve of Eratosthenes algorithm for finding prime numbers

#include <stdio.h>

int main()					// begin function main
{
	int n = 1000;
	int a[1000] = {0};			// initialize all values of array a to 0
	int i,p,r,col=1;


	for (p=2;p<n;p++) {			
		for (i=p+1;i<n;i++) {
			r = i % p;		// determine remainder of i/p
			if (r == 0){		// if factor, set a[i] = 1
				a[i] = 1;
			}
		}
	}

	a[0] = 1;
	a[1] = 1;	

	for (i=0;i<n;i++) {			// loop through array, determine which
		if (a[i] == 0) {		// #s are still = 0
			printf("%5d ",i);	// print subscript of array element
			col++;
			if (col>10) {
				printf("\n");
				col = 1;
			}
		}
	}

	printf("\n");
	return 0;

} 						// end function main
