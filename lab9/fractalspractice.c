// fractals.c
// 11/22/15
// Author: Taylor Rongaus
// This program will generate a variety of different fractal shapes, displayed when the user presses any of the numbers 1 through 8

#include <stdio.h>
#include "gfx4.h"
#include <math.h>


// prototypes
void drawtriangle(int x1, int y1, int x2, int y2, int x3, int y3);
void drawsquare(int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4);
void sierpinski(int x1, int y1, int x2, int y2, int x3, int y3 );
void shrinkingsquares(int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4, int xsize, int ysize);
void spiralsquares(int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4, int xsize, int ysize, int scale3);
void circularlace(double x1, double y1, double r);
void snowflake(int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4, int x5, int y5, int x6, int y6, int scale5);
//void snowflake2(double x[], double y[], int scale);
void tree(int x1, int y1, int x2, int y2, int scale6);
void fern(int x1, int y1, int x2, int y2, int scale7);
void snowflake3(int x1, int y1, int scale5, double theta5);




// begin main execution
int main() {
	int xsize = 800;
	int ysize = 800;
	char c;	
	int scale3 = 100;
	int scale5 = 200;
	int scale6 = 400;
	int scale7 = 400;
	double theta5 = 72*M_PI/180;
	double x[6] = {xsize/2,(xsize/2)+scale5*cos(72*2*M_PI/180),(xsize/2)+scale5*cos(144*2*M_PI/180),(xsize/2)+scale5*cos(216*2*M_PI/180),(xsize/2)+scale5*cos(288*2*M_PI/180), (xsize/2)+scale5*cos(2*2*M_PI)};
	double y[6] = {ysize/2,(ysize/2)+scale5*sin(72*2*M_PI/180),(ysize/2)+scale5*sin(144*2*M_PI/180),(ysize/2)+scale5*sin(216*2*M_PI/180),(ysize/2)+scale5*sin(288*2*M_PI/180), (ysize/2)+scale5*sin(2*2*M_PI)};

	gfx_open(xsize, ysize, "fractals.c");
	while (1) {
	c = gfx_wait();
	gfx_color(255,255,255);
	switch(c){
		case '1':
			gfx_clear();
			sierpinski(20,20,xsize-20,20,xsize/2,ysize-20);
			break;
		case '2':
			gfx_clear();
			shrinkingsquares(xsize/4,ysize/4,3*xsize/4,ysize/4,3*xsize/4,3*ysize/4,xsize/4,3*ysize/4, xsize, ysize);
			break;
		case '3':
			gfx_clear();
			spiralsquares(xsize/4,3*ysize/4,xsize/4+scale3,3*ysize/4,xsize/4+scale3,3*ysize/4+scale3,xsize/4,3*ysize/4+scale3,xsize,ysize,scale3);
			break;
		case '4':
			gfx_clear();
			circularlace(xsize/2,ysize/2,250);
			break;
		case '5':
			gfx_clear();
			// snowflake2(x[5], y[5], scale5);
			// snowflake(xsize/2,ysize/2,(xsize/2)+(scale5)*cos(72*2*M_PI/180),(ysize/2)+(scale5)*sin(72*2*M_PI/180),(xsize/2)+(scale5)*cos(144*2*M_PI/180),(ysize/2)+(scale5)*sin(144*2*M_PI/180),(xsize/2)+(scale5)*cos(216*2*M_PI/180),(ysize/2)+(scale5)*sin(216*2*M_PI/180),(xsize/2)+(scale5)*cos(288*2*M_PI/180),(ysize/2)+(scale5)*sin(288*2*M_PI/180),(xsize/2)+(scale5)*cos(2*2*M_PI),(ysize/2)+(scale5)*sin(2*2*M_PI),scale5);
			snowflake3(xsize/2, ysize/2, scale5, theta5);
			break;
		case '6':
			gfx_clear();
			tree(xsize/2,ysize-60,xsize/2,ysize-60-scale6,scale6);
			break;
		case '7':
			gfx_clear();
			fern(xsize/2,ysize-60,xsize/2,ysize-60-scale7,scale7);
			break;
		case '8':
			gfx_clear();
			break;
	c = '+';
	}
	
	if(c == 'q') break;
	}
}










// begin function execution



// function to draw a triangle using gfx4
void drawtriangle(int x1, int y1, int x2, int y2, int x3, int y3) {
	gfx_line(x1, y1, x2, y2);
	gfx_line(x2, y2, x3, y3);
	gfx_line(x3, y3, x1, y1);
}

// function to draw a square using gfx4
void drawsquare(int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4){
	gfx_line(x1,y1,x2,y2);
	gfx_line(x2,y2,x3,y3);
	gfx_line(x3,y3,x4,y4);
	gfx_line(x4,y4,x1,y1);
}

// recursive function to generate the Sierpinski triangle fractal
void sierpinski( int x1, int y1, int x2, int y2, int x3, int y3 ) {
   // base case
   if(abs(x2-x1)<2) return;
   // draw first triangle
   drawtriangle(x1, y1, x2, y2, x3, y3);
   // recursive calls
   sierpinski(x1, y1, (x1+x2)/2, (y1+y2)/2, (x1+x3)/2, (y1+y3)/2 );
   sierpinski((x1+x2)/2, (y1+y2)/2, x2, y2, (x2+x3)/2, (y2+y3)/2 );
   sierpinski((x1+x3)/2, (y1+y3)/2, (x2+x3)/2, (y2+y3)/2, x3, y3 );
}

// recursive function to generate the shrinking squares fractal
void shrinkingsquares(int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4, int xsize, int ysize){
	// base case
	if(abs(x2-x1)<15) return;
	
	// draw first square
	drawsquare(x1,y1,x2,y2,x3,y3,x4,y4);
	
	// recursive calls
	shrinkingsquares(x1/2,y1/2,x2/2,y2/2,x3/2,y3/2,x4/2,y4/2,xsize,ysize);
	shrinkingsquares(xsize-x1/2,y1/2,xsize-x2/2,y2/2,xsize-x3/2,y3/2,xsize-x4/2,y4/2,xsize,ysize);
	shrinkingsquares(xsize-x1/2,ysize-y1/2,xsize-x2/2,ysize-y2/2,xsize-x3/2,ysize-y3/2,xsize-x4/2,ysize-y4/2,xsize,ysize);
	shrinkingsquares(x1/2,ysize-y1/2,x2/2,ysize-y2/2,x3/2,ysize-y3/2,x4/2,ysize-y4/2,xsize,ysize);
}



// recursive function to generate the spiral squares fractal
void spiralsquares(int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4, int xsize, int ysize, int scale3) {
	// base case
	if (abs(x2-x1)<5) return;
	
	// draw first square
	drawsquare(x1,y1,x2,y2,x3,y3,x4,y4);

	// recursive calls
	

}



// recursive function to generate the circular lace fractal
void circularlace(double x1, double y1, double r) {
	// base case
	if (r<1) return;
	// draw first circle
	gfx_circle(x1,y1,r);
	// recursive calls
	int i;
	for(i=1;i<=6;i++) {
		circularlace(x1+r*cos(i*M_PI/3),y1+r*sin(i*M_PI/3),0.33*r);
	}
}



// recursive function to generate the snowflake fractal
void snowflake(int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4, int x5, int y5, int x6, int y6, int scale5) {
	// base case
	if ((abs(x2-x1)<2) || (scale5<=0)) return;
	
	// draw first snowflake
	gfx_line(x1,y1,x2,y2);
	gfx_line(x1,y1,x3,y3);
	gfx_line(x1,y1,x4,y4);
	gfx_line(x1,y1,x5,y5);
	gfx_line(x1,y1,x6,y6);
	
	// recursive calls
	// lmao sry
/*	scale5 = 0.35*scale5;
	int i, j;
	for (i=2; i<=6; i++){
		snowflake(xi,yi,xi+scale5*cos(72*2*M_PI/180),yi+scale5*sin(72*2*M_PI/180),xi+scale5*cos(144*2*M_PI/180),yi+scale5*sin(144*2*M_PI/180),xi+scale5*cos(216*2*M_PI/180),yi+scale5*sin(216*2*M_PI/180),xi+scale5*cos(288*2*M_PI/180),yi+scale5*sin(288*2*M_PI/180),xi+scale5*cos(2*2*M_PI),yi+scale5*sin(2*2*M_PI),scale5);
		
	}
	
/*	snowflake(x2,y2,x2+scale5*cos(72*2*M_PI/180),y2+scale5*sin(72*2*M_PI/180),x2+scale5*cos(144*2*M_PI/180),y2+scale5*sin(144*2*M_PI/180),x2+scale5*cos(216*2*M_PI/180),y2+scale5*sin(216*2*M_PI/180),x2+scale5*cos(288*2*M_PI/180),y2+scale5*sin(288*2*M_PI/180),x2+scale5*cos(2*2*M_PI),y2+scale5*sin(2*2*M_PI),scale5);
	snowflake(x3,y3,x3+scale5*cos(72*2*M_PI/180),y3+scale5*sin(72*2*M_PI/180),x3+scale5*cos(144*2*M_PI/180),y3+scale5*sin(144*2*M_PI/180),x3+scale5*cos(216*2*M_PI/180),y3+scale5*sin(216*2*M_PI/180),x3+scale5*cos(288*2*M_PI/180),y3+scale5*sin(288*2*M_PI/180),x3+scale5*cos(2*2*M_PI),y3+scale5*sin(2*2*M_PI),scale5);
	snowflake(x4,y4,x4+scale5*cos(72*2*M_PI/180),y4+scale5*sin(72*2*M_PI/180),x4+scale5*cos(144*2*M_PI/180),y4+scale5*sin(144*2*M_PI/180),x4+scale5*cos(216*2*M_PI/180),y4+scale5*sin(216*2*M_PI/180),x4+scale5*cos(288*2*M_PI/180),y4+scale5*sin(288*2*M_PI/180),x4+scale5*cos(2*2*M_PI),y4+scale5*sin(2*2*M_PI),scale5);
	snowflake(x5,y5,x5+scale5*cos(72*2*M_PI/180),y5+scale5*sin(72*2*M_PI/180),x5+scale5*cos(144*2*M_PI/180),y5+scale5*sin(144*2*M_PI/180),x5+scale5*cos(216*2*M_PI/180),y5+(scale5)*sin(216*2*M_PI/180),x5+scale5*cos(288*2*M_PI/180),y5+scale5*sin(288*2*M_PI/180),x5+scale5*cos(2*2*M_PI),y5+scale5*sin(2*2*M_PI),scale5);
	snowflake(x6,y6,x6+scale5*cos(72*2*M_PI/180),y6+scale5*sin(72*2*M_PI/180),x6+scale5*cos(144*2*M_PI/180),y6+scale5*sin(144*2*M_PI/180),x6+scale5*cos(216*2*M_PI/180),y6+scale5*sin(216*2*M_PI/180),x6+scale5*cos(288*2*M_PI/180),y6+scale5*sin(288*2*M_PI/180),x6+scale5*cos(2*2*M_PI),y6+scale5*sin(2*2*M_PI),scale5);
*/
}



/*void snowflake2(double x[], double y[], int scale5) {
	// base case
	if ((abs(x[2]-x[1])<2) || (scale5<=0)) return;
	
	// draw first snowflake
	gfx_line(x[1],y[1],x[2],y[2]);
	gfx_line(x[1],y[1],x[3],y[3]);
	gfx_line(x[1],y[1],x[4],y[4]);
	gfx_line(x[1],y[1],x[5],y[5]);
	gfx_line(x[1],y[1],x[6],y[6]);

	// recursive calls
	scale5 = 0.35*scale5;
	int i, j;
	for (i=1; i<=5; i++){
		snowflake(x[i],y[i],x[i]+scale5*cos(72*2*M_PI/180),y[i]+scale5*sin(72*2*M_PI/180),x[i]+scale5*cos(144*2*M_PI/180),y[i]+scale5*sin(144*2*M_PI/180),x[i]+scale5*cos(216*2*M_PI/180),y[i]+scale5*sin(216*2*M_PI/180),x[i]+scale5*cos(288*2*M_PI/180),y[i]+scale5*sin(288*2*M_PI/180),x[i]+scale5*cos(2*2*M_PI),y[i]+scale5*sin(2*2*M_PI),scale5);
			
	}

}*/

// ^ how do I format this correctly? just want to get rid of the disgusting length of the working code

// recursive function to generate the tree fractal
void tree(int x1, int y1, int x2, int y2, int scale6) {

	double linelength = sqrt((pow(x2-x1,2)+pow(y1-y2,2)));
	
	// base case
	if ((linelength<2) || (scale6<=0)) return;
	
	// draw first line
	gfx_line(x1, y1, x2, y2);
	
	// recursive calls
	// shrink scale
	scale6 = 0.75*scale6;
	

}


// recursive function to generate the fern fractal
void fern(int x1, int y1, int x2, int y2, int scale7) {
	double linelength = sqrt((pow(x2-x1,2)+pow(y1-y2,2)));
	
	// base case
	if ((linelength<2) || (scale7<=0)) return;
	
	// draw first line
	gfx_line(x1, y1, x2, y2);
	
	// recursive calls
	// shrink scale
	scale7 = 0.25*scale7;
	// split into two branches at each of 4 junctions
/*	int i = 4;
	for (i=1;i<=4;i++) {
		fern(x2,i/4*y2,(x2-(scale7)),i/4*(y2-scale7),scale7);
		fern(x2,i/4*y2,(x2+(scale7)),i/4*(y2-scale7),scale7);
	}
*/	// ^ why isn't this working?

}




void snowflake3(int x1, int y1, int scale5, double theta5) {
	int i;
	if (scale5<3) return;
	
	for (i=1;i<6;i++) 
		gfx_line(x1,y1,x1+scale5*cos(i*theta5),y1+scale5*sin(i*theta5));
	
	for (i=1;i<6;i++)
		snowflake3(x1+scale5*cos(i*theta5),y1+scale5*sin(i*theta5),scale5*0.35,theta5);

}




