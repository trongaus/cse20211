// fractals.c
// 11/24/15
// Author: Taylor Rongaus
// This program will generate a variety of different fractal shapes, displayed when the user presses any of the numbers 1 through 7

#include <stdio.h>
#include "gfx4.h"
#include <math.h>

// prototypes
void drawtriangle(int x1, int y1, int x2, int y2, int x3, int y3);
void drawsquare(int xcent, int ycent, int length);
void sierpinski(int x1, int y1, int x2, int y2, int x3, int y3 );
void shrinkingsquares(int xcent, int ycent, int length2);
void spiralsquares(int x1, int y1, double scale3, double r3, double theta3);
void circularlace(double x1, double y1, double r);
void snowflake(int x1, int y1, int scale5, double theta5);
void tree(int x1, int y1, double scale6, double theta6);
void fern(int x1, int y1, double scale7, double theta7);

// begin main execution
int main() {
	// window dimensions
	int xsize = 800;
	int ysize = 800;
	// center point of window
	double xcent = xsize/2;
	double ycent = ysize/2;
	// variables for each fractal, where the # represents the key # pressed to generate
	int length2 = 150;
	int r3 = 0;
	double scale3 = 6;
	double scale5 = 200;
	double scale6 = 250;
	double scale7 = 500;
	double theta3 = 0;
	double theta5 = 72*M_PI/90;
	double theta6 = M_PI/2;
	double theta7 = M_PI/2;
	char c;	
	gfx_open(xsize, ysize, "fractals.c");
	while (1) {
	c = gfx_wait();
	gfx_color(255,255,255);
	// call each function upon corresponding number pressed
	switch(c){
		case '1':
			gfx_clear();
			sierpinski(20,20,xsize-20,20,xcent,ysize-20);
			break;
		case '2':
			gfx_clear();
			shrinkingsquares(xcent,ycent,length2);
			break;
		case '3':
			gfx_clear();
			spiralsquares(xcent,ycent,scale3,r3,theta3);	
			break;
		case '4':
			gfx_clear();
			circularlace(xcent,ycent,250);
			break;
		case '5':
			gfx_clear();
			snowflake(xcent, ycent, scale5, theta5);
			break;
		case '6':
			gfx_clear();
			tree(xcent,ysize-60,scale6,theta6);
			break;
		case '7':
			gfx_clear();
			fern(xcent,ysize-60,scale7,theta7);
			break;
	// reset c to an impossible value
	c = '+';
	}
	// if c = q, quit
	if(c == 'q') break;
	}
}


// begin function execution

// function to draw a triangle using gfx4
void drawtriangle(int x1, int y1, int x2, int y2, int x3, int y3) {
	gfx_line(x1, y1, x2, y2);
	gfx_line(x2, y2, x3, y3);
	gfx_line(x3, y3, x1, y1);
}

// function to draw a square using gfx4
void drawsquare(int xcent, int ycent, int length) {
	gfx_line(xcent-length,ycent-length,xcent+length,ycent-length);
	gfx_line(xcent+length,ycent-length,xcent+length,ycent+length);
	gfx_line(xcent+length,ycent+length,xcent-length,ycent+length);
	gfx_line(xcent-length,ycent+length,xcent-length,ycent-length);
}

// recursive function to generate the Sierpinski triangle fractal
void sierpinski(int x1, int y1, int x2, int y2, int x3, int y3) {
   // base case
   if(abs(x2-x1)<2) return;
   // draw first triangle
   drawtriangle(x1, y1, x2, y2, x3, y3);
   // recursive calls
   sierpinski(x1, y1, (x1+x2)/2, (y1+y2)/2, (x1+x3)/2, (y1+y3)/2 );
   sierpinski((x1+x2)/2, (y1+y2)/2, x2, y2, (x2+x3)/2, (y2+y3)/2 );
   sierpinski((x1+x3)/2, (y1+y3)/2, (x2+x3)/2, (y2+y3)/2, x3, y3 );
}

// recursive function to generate the shrinking squares fractal
void shrinkingsquares(int xcent, int ycent, int length2){
	// base case
	if(length2<3) return;
	// draw first square
	drawsquare(xcent,ycent,length2);
	// recursive calls
	shrinkingsquares(xcent-length2,ycent-length2,length2/2.2);
	shrinkingsquares(xcent+length2,ycent-length2,length2/2.2);
	shrinkingsquares(xcent+length2,ycent+length2,length2/2.2);
	shrinkingsquares(xcent-length2,ycent+length2,length2/2.2);
}

// recursive function to generate the spiral squares fractal
void spiralsquares(int xcent, int ycent, double scale3, double r3, double theta3) {
	double xi = 0, yi = 0;
	// base case
	if (r3>210) return;
	// draw first square
	drawsquare(xcent,ycent,scale3);
	// recursive calls
	xi = (r3*cos(theta3));
	yi = (r3*sin(theta3));
	spiralsquares(xcent+xi,ycent+yi,1.08*scale3,r3+7,theta3+M_PI/6);
}


// recursive function to generate the circular lace fractal
void circularlace(double x1, double y1, double r) {
	// base case
	if (r<1) return;
	// draw first circle
	gfx_circle(x1,y1,r);
	// recursive calls
	int i;
	for(i=1;i<=6;i++) {
		circularlace(x1+r*cos(i*M_PI/3),y1+r*sin(i*M_PI/3),0.33*r);
	}
}

// recursive function to generate the snowflake fractal
void snowflake(int x1, int y1, int scale5, double theta5) {
	int i;
	// base case
	if (scale5<3) return;
	// draw first snowflake
	for (i=1;i<6;i++) 
		gfx_line(x1,y1,x1+scale5*cos(i*theta5),y1+scale5*sin(i*theta5));
	// recursive calls
	for (i=1;i<6;i++)
		snowflake(x1+scale5*cos(i*theta5),y1+scale5*sin(i*theta5),scale5*0.35,theta5);
}

// recursive function to generate the tree fractal
void tree(int x1, int y1, double scale6, double theta6) {
	double x2, y2;
	x2 = x1-scale6*cos(theta6);
	y2 = y1-scale6*sin(theta6);
	// base case
	if (scale6<=3) return;
	// draw first line
	gfx_line(x1, y1, x2, y2);
	// recursive calls
	tree(x2,y2,scale6*0.68,theta6+M_PI/6);
	tree(x2,y2,scale6*0.68,theta6-M_PI/6);
}

// recursive function to generate the fern fractal
void fern(int x1, int y1, double scale7, double theta7) {
	double x2, y2;
	x2 = x1-scale7*cos(theta7);
	y2 = y1-scale7*sin(theta7);
	// base case
	if ((scale7<=5)) return;
	// draw first line
	gfx_line(x1, y1, x2, y2);
	// recursive calls
	int dy;
	for (dy=0.1*scale7;dy<scale7;dy+=scale7/4) {
		fern(x2+dy*cos(theta7),y2+dy*sin(theta7),0.35*scale7,theta7+M_PI/5);
		fern(x2+dy*cos(theta7),y2+dy*sin(theta7),0.35*scale7,theta7-M_PI/5);
	}
}

