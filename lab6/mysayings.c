// mysayings.c
// Author: Taylor Rongaus
// 10/15/15
// This program that will store and manage many of my favorite sayings

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#define MAXSAYINGS 100
#define MAXSAYINGSLENGTH 200

// prototypes
void displayall(char sayings[][MAXSAYINGSLENGTH], int j);
void sayingsearch(char sayings[][MAXSAYINGSLENGTH], int j);
void newfilesave(char sayings[][MAXSAYINGSLENGTH], int j);
void newsaying(char sayings[][MAXSAYINGSLENGTH], int j);


int main ()										// begin function main
{
	FILE *ifp;									// file pointers
	char sayings[MAXSAYINGS][MAXSAYINGSLENGTH]; // stores each saying in a 2D array
	int namelength = 30;						// max length of sayings file name
	char startup[namelength];					// stores the name of the sayings file
	int endprogram = 0;							// endprogram while loop end check
	int checkchoice = 0;						// valid choice if statement check
	int num, i, j = 0;
	
	printf("Enter sayings file name: ");		// read in sayings file name from user
	scanf("%s", startup);
	ifp = fopen(startup, "r");					// open file entered
	
	if (!ifp) {									// ensure file name is valid
		printf("Error in opening file %s\n",startup);
		return 0;
	}
	
	for (i=0; i<=MAXSAYINGS; i++) {				// read in sayings up until max # of
		if (!feof(ifp)) {						// sayings until EOF is reached
			fgets(sayings[i], MAXSAYINGSLENGTH, ifp);
			++j;
		}
	}

	while (endprogram == 0) {
		while (checkchoice == 0) {
			printf("\n1: display all sayings currently in the database\n2: enter a new saying into the database\n3: list sayings that contain a given word entered by the user\n4: save all sayings in a new text file\n5: quit the program\n");
			printf("Enter your choice: ");
			scanf("%d", &num);
	  		if (num == 1 || num == 2 || num == 3 || num == 4 || num == 5) {
	      		checkchoice = 1;
	    		}
	  		else {
	      		printf ("Error. Please enter a valid choice.\n");
	   		}
	  		printf ("\n");
		}
		switch (num) {
			case 1:
				displayall(sayings, j);				// execute function displayall
				checkchoice = 0;
				break;
			case 2:
				newsaying(sayings, j);				// execute function newsaying
				j++;
				checkchoice = 0;
				break;
			case 3:
				sayingsearch(sayings, j);			// execute funtion sayingsearch
				checkchoice = 0;
				break;
			case 4:
				newfilesave(sayings, j);			// execute function newfilesave
				checkchoice = 0;
				break;
			case 5:
				printf ("Good-bye!\n");
				endprogram = 1;
				break;
		}
		
	}
	
}


void displayall(char sayings[][MAXSAYINGSLENGTH], int j) {
	int i;
	for (i=0;i<j;i++) {
		printf("%s",sayings[i]);
	}

}

void newsaying(char sayings[][MAXSAYINGSLENGTH], int j) {
	int i;
	printf("Enter a saying to add to the database: ");
	getchar();
	fgets(sayings[j], MAXSAYINGSLENGTH, stdin);
	printf("The saying has been added.\n");
}

void sayingsearch(char sayings[][MAXSAYINGSLENGTH], int j) {
	int i;
	char word[15];
	printf("Enter a word to search the sayings for: ");
	scanf("%s", word);
	printf("\n");
	for (i=0;i<j;i++){
		if (strcasestr(sayings[i],word)) {
			printf("%s", sayings[i]);
		}
	}

}

void newfilesave(char sayings[][MAXSAYINGSLENGTH], int j){
	FILE *ofp;
	int i;
	char outfile[20];
	printf("Enter the name of the new text file: ");
	scanf("%s",outfile);
	printf("\n");
	ofp = fopen(outfile, "w");
	for (i=0;i<j;i++) {
		fprintf(ofp, sayings[i]);
	}
	printf("Your file has been saved.\n");
}




