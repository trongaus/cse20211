// table.c
// Author: Taylor Rongaus
// 9/7/15
// Collaborated w/ Maddie Gleason

// this program displays an integer multiplication table

#include <stdio.h>
int main(void)
{
	int row; //size of table on x axis
	int col; //size of table on y axis

	printf("Enter the number of rows of the table\n");
	scanf("%d", &row); //read x dimension

	printf ("Enter the number of columns of the table\n");
	scanf("%d", &col); //read y dimension

	printf("*"); // display *

	int i;
		i = 1;
	int j; // define counter variables
		j = 1;

	while (i<=row) { // display 0th row
		printf("%4d ",i);
		i = i + 1;
		}

	printf("  \n");

	for (j=1; j<=col; ++j) {
		for (i=1; i<=row; ++i) {
			if (i == 1) { // display 0th column
				printf("%d ",j);
			}
			printf("%3d  ",i*j); // multiple i & j to fill in table
		}
			printf("\n");
	}

} // end function main
