// mortgage.c
// Author: Taylor Rongaus
// 9/13/15

// this program will calculate the amount of time it will take to pay off a 
// loan when the principle, yearly interest rate, and desired monthly 
// payment are inputted

#include <stdio.h>
int main(void)
{
	int principle;
	double interest;
	double payment;
	double balance;
	double intamt;
	double paymentsum;
	int years;
	int month;
	int check; // introduce variable to check for potential improper inputs

	check = 0;
	while (check == 0) {
		printf("Enter the principle: $");
		scanf("%d", &principle);
		if (principle >= 0) {
			check = 1;}
		else{
			printf("Error: please enter a positive value for the principle\n");
		}
	}

	check = 0;
	while (check == 0) {
		printf("Enter the yearly interest rate (percent): ");
		scanf("%lf", &interest);
		if (interest >= 0) {
			check = 1; }
		else {
			printf("Error: please enter a positive value for the yearly interest rate\n");
		}
	}

	check = 0;
	while (check == 0) {
		printf("Enter the desired monthly payment: $");
		scanf("%lf", &payment);
		if (payment >= 0) {
			check = 1; }
		else {
			printf("Error: please enter a positive value for the monthly payment\n");
		}
	}

	printf("\nMonth       Payment($)       Interest($)       Balance($)\n");
	// display table header

	interest = interest/100; // turn percent into decimal
	month = 0; // initialize variables
	balance = principle;
	while (balance >= payment) { // ensure no negative balance
		intamt = (interest/12)*balance;
		balance = intamt + balance - payment;
		month = month + 1;

		if (intamt > payment){
			printf("Error: improper inputs. Please try again.\n");
			return 0;
		}

		printf("%d",month);
		if (month < 10){
			printf("%17.2lf",payment);
		}
		if (month > 9 && month < 100){
			printf("%16.2lf",payment);
		}
		if (month > 99 && month < 1000) {
			printf("%15.2lf",payment);
		}

		printf("%15.2lf",intamt);
		printf("%20.2lf\n",balance);

		paymentsum = paymentsum + payment;
	}


if (balance < payment) { // for last payment when balance < desired payment
	intamt = (interest/12)*balance;
	payment = balance + intamt;
	balance = intamt + balance - payment;
	month = month + 1;
	printf("%d",month);
		if (month < 10){
			printf("%17.2lf",payment);
		}
		if (month > 9 && month < 100){
			printf("%16.2lf",payment);
		 }
		if (month > 99 && month < 1000) {
			printf("%15.2lf",payment);
		}

	printf("%15.2lf",intamt);
	printf("%20.2lf\n",balance);

	paymentsum = paymentsum + payment;
}

years = month/12;
month = month % 12;
printf("You paid a total of $%.2lf over %d years and %d months.\n",paymentsum,years,month);

} // end function main
