// graph.c
// Author: Taylor Rongaus
// 9/13/15
// Collaborated w/ Maddie Gleason

// this program generates a simple ASCII Art Graph of a mathematical function

#include <stdio.h>
#include <math.h>

int main(void)
{

	double x;
	double y;
    double max;
    double min;
    double xmax;
    double xmin;
    double pi;
		max = 0; // set reasonable original min and max values
		min = 20;

	x = 0;
	printf("  X \t  Y\n");

	for (x = -2.24; x <= 2.02; x=x+0.02) {
		pi = 4*atan(1);
		y = 10*fabs((exp(-x) * cos(2*pi*x))); // determine function
		printf("%.2lf\t",x); // display x and y value columns
		printf("%.2lf\t",y);
		int p;

		if (y>max) { // check each y value to determine if min or max
			max = y;
			xmax = x;
		}

		if (y<min) {
			min = y;
			xmin = x;
		}

		p = y; // turn y into an integer p
			if (p<1) { // ensure # is displayed for values between 0 & 1
				p = 1;
			}

		for (int i = 1; i <= p; i++) {
			printf("#"); // display corresponding number of #
		}

	printf("\n");
	}

printf("\n");
printf("The maximum value of %.2lf occurred at x = %.2lf\n",max,xmax);
printf("The minimum value of %.2lf occurred at x = %.2lf\n",min,xmin);

} // end function main
