// letterfreq.c
// Author: Taylor Rongaus
// 10/8/15
// This program counts the frequency of each letter of the alphabet as they occur in a large text contained in a text file.

#include <stdio.h>
#include <ctype.h>

int main() 						// begin function main
{
	FILE *ifp;
	char infile[20];	
	char c;						// read in value
	int lettercount = 0;		// letter, space, char counters
	int spacecount = 0;
	int totalcharcount = 0;
	int i,j = 0;				// counter variables
	int n[26] = {0};			// letter tally array
	double spacepercent = 0.0;

	printf("Enter data file name: ");
	scanf("%s",&infile);
	
	ifp = fopen(infile, "r");
	if (!ifp) {
		printf("error in opening file %s\n", infile);
		return 1;
	}
	
	while (fscanf(ifp,"%c",&c) != EOF) {
		totalcharcount++;
		if (isupper(c) || islower(c)) {
			lettercount++;
			if (isupper(c)) {
				n[c - 'A']++;
			}
			if (islower(c)){
				n[c - 'a']++;
			
			}
		}
		else if (isspace(c)) {
			spacecount++;
		}
	}
	
	for (i=0;i<26;i++) {
		j = i + 97;
		printf("%c: %d\n", j, n[i]);
	}
	
	spacepercent = ((double)spacecount/((double)totalcharcount)) * 100;
	
	printf("There are %d total letters\n", lettercount);
	printf("There are %d total chatacters\n", totalcharcount);
	printf("There are %d white space characters\n", spacecount);
	printf("Space percentage: %.1lf\n", spacepercent);
	
	
} 								// end function main

