#include "newpolarfn.h"
#include <stdio.h>
#include <math.h>

int polar_info(double x, double y, double *radius, double *theta) 
{
	*radius = sqrt ((x * x) + (y * y));
	*theta = atan2 (y, x) * 180 / M_PI;
	
	int location;

	if ((x == 0.0) && (y == 0.0))
	{
		location = 0;		// 0 represents at the origin
	}
	if ((x > 0.0) && (y > 0.0))
	{
		location = 1;		// 1 represents in quadrant 1
	}
	if ((x < 0.0) && (y > 0.0))
	{
		location = 2;		// 2 represents in quadrant 2
	}
	if ((x < 0.0) && (y < 0.0))
	{
		location = 3;		// 3 represents in quadrant 3
	}
	if ((x > 0.0) && (y < 0.0))
	{
		location = 4;		// 4 represents in quadrant 4
	}
	if ((x != 0.0) && (y == 0.0))
	{
		location = -1;		// -1 represents on the x-axis
	}
	if ((x == 0.0) && (y != 0.0))
	{
		location = -2;		// -2 represents on the y-axis
	}

	return location;

}
