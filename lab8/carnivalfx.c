// carnival.c
// 11/1/15
// Author: Taylor Rongaus
// This program will create a fun carnival ride with four different axes of motion.

#include <stdio.h>
#include "gfx2.h"
#include <math.h>

// note: separating each axis of motion into a separate function caused the animation to lag. As per Ramzi's instructions, I wrote the four motions into a function called drawscreen, incremented the necessary variables outside in main, and passed in the necessary variables for each iteration
 
void drawscreen(int xsize, int ysize, int dx, int dy, int directionx, int directiony, int r1, double dtheta1, double dtheta2);

int main () {

	int xsize = 800;
	int ysize = 800;
	int dt = 8000;
	char c;
	int dx = 0;
	int dy = 0;
	int directionx = 1;
	int directiony = 1;
	int r1 = 80;
	double dtheta1 = 0.0;
	double dtheta2 = 0.0;
	
	gfx_open(xsize,ysize,"animate.c");
	drawscreen(xsize, ysize, dx, dy, directionx, directiony, r1, dtheta1, dtheta2);
	
	while (1){
	
		//clear
		gfx_clear();
		drawscreen(xsize, ysize, dx, dy, directionx, directiony, r1, dtheta1, dtheta2);

		//increment thetas
		dtheta1 = dtheta1 + (2*M_PI/360);
		dtheta2 = dtheta2 - (2*M_PI/360);

		// switch directions at end of display window
		if (directionx)
			dx++;
		else 
			dx--;
		if (directiony)	
			dy = dy+2;
		else
			dy = dy-2;

		// check to see if too close to window edge
		if ((xsize/2+dx+r1) >= xsize){
			directionx = 0;
		}
		else if ((xsize/2+dx-r1) <= 0){
			directionx = 1;
		}
		if ((ysize/2+dy+r1) >= ysize){
			directiony = 0;
		}
		else if ((ysize/2+dy-r1) <= 50){
			directiony = 1;
		}

		// flush, usleep, and check to see if c = q (quit)
		gfx_flush;
		usleep(dt);

		if (gfx_event_waiting()) {	
			c = gfx_wait();    
			if (c == 'q') break; 
		}
	
	
	}


}

void drawscreen(int xsize, int ysize, int dx, int dy, int directionx, int directiony, int r1, double dtheta1, double dtheta2) {

	int rc2 = -250;	
	int rc3 = -100;	
	int r2 = 40;
	int r3 = 20;
	
	// define dummy variables for commonly used code
	int a = (xsize/2) + dx;
	int b = (ysize/2) + dy;
	int c = (rc2*cos(dtheta1));
	int d = (rc2*sin(dtheta1));
	int e = (rc3*cos(dtheta2));
	int f = (rc3*sin(dtheta2));

	// draw horizontal line
	gfx_color(0,255,0);
	gfx_line(0,50,xsize,50);
	
	//draw circle 1 and line
	gfx_color(255,255,255);
	gfx_line(a,50,a,b);
	gfx_circle(a,b,r1);
	
	//draw circle 2 and line
	gfx_color(0,0,255);
	gfx_line(a,b,a+c,b+d);
	gfx_circle(a+c,b+d,r2);
	
	//draw circle 3 and line
	gfx_color(255,0,0);
	gfx_line(a+c,b+d,a+c+e,b+d+f);
	gfx_circle(a+c+e,b+d+f,r3);

}



