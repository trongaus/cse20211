
// graphcalc.c
// 11/6/15
// Author: Taylor Rongaus
// This program will draw a detailed graph of the Taylor-series expansion of sin(x), where the user will control how many terms are being used in the graph.

#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <stdlib.h>
#include "gfx2.h"

// prototypes
void displayaxes(int xsize, int ysize, int s);
double getfact(int n);
char *num2str(int n);
double calctaylor(double x2, int num_terms);
void displaygraph(int xsize, int ysize, int s, int num_terms, double x1, double x2, double approx);

// begin main execution
int main () {

	int xsize = 630;								// xsize of window
	int ysize = 630;								// ysize of window
	int s = 30;										// scale to size of window and axes
	char c;
	int num_terms = 0;								// initial number of Taylor terms
	double approx;
	double x1 = -xsize/2;
	double x2 = -xsize/2;
	
	gfx_open(xsize,ysize,"graphcalc.c");			// initially display graph y=x
	displaygraph(xsize, ysize, s, num_terms, x1, x2, approx);
	
	while (1) {
		
		gfx_flush();								// flush window
		
		if (gfx_event_waiting()) {	
			c = gfx_wait();							// wait for the user to press a key
			if (c == '-'){							// '-' decrements number of terms
				if (num_terms != 0) {				// and redraws graph
					num_terms--;
					gfx_clear();
					displaygraph(xsize, ysize, s, num_terms, x1, x2, approx);
				}
			}
			if (c == '='){							// '=' increments number of terms
				num_terms++;						// and redraws graph
				gfx_clear();
				displaygraph(xsize, ysize, s, num_terms, x1, x2, approx);
			}
			if (c == 'q') break;					// if q, quit
		}
	}
}


// begin function execution
void displayaxes(int xsize, int ysize, int s) {
	int i;
	gfx_color(0,150,150);							// display axes
	gfx_line(xsize/2,0,xsize/2,ysize);
	gfx_line(0,ysize/2,xsize,ysize/2);
	for (i=-10; i<=10; i++){						// display tick marks and text	
		gfx_line(xsize/2+(s*i),ysize/2-5,xsize/2+(s*i),ysize/2+5);
		gfx_line(xsize/2-5,ysize/2+(s*i),xsize/2+5,ysize/2+(s*i));
		gfx_text(xsize/2+(s*i)+4,ysize/2-10,num2str(i));
		gfx_text(xsize/2-20,ysize/2+(s*i),num2str(-1*i));
	}

}

char *num2str(int n) {								// convert int to string
  static char a[20], *p = a;
  snprintf(p, 20, "%d", n);
  return p;
}

double getfact(int n) {								// calculate factorial
	double fact = 1; 
	int i;
	for (i=1;i<=n;i++) {
		fact = fact * i;
	}
	return fact;
}

double calctaylor(double x2, int num_terms) {		// calculate Taylor approximation
	int n;
	double approx = 0.0;
	for (n=0;n<=num_terms;n++){
		approx = approx + (pow(-1,n)*pow(x2,2*n+1)/getfact(2*n+1));
	}
	return approx;
}

void displaygraph(int xsize, int ysize, int s, int num_terms, double x1, double x2, double approx) {
	displayaxes(xsize,ysize,s);						// begin function displayaxes
	gfx_color(255,255,255);							// graph sine function
	while (x1 <= xsize) {
		gfx_point(s*x1+xsize/2,(-s*sin(x1))+(ysize/2));
		x1 = x1 + 0.01;
	}
	gfx_color(255,255,0);	
	while (x2<= xsize) {							// graph Taylor approx of sine
		approx = calctaylor(x2,num_terms);			// begin function calctaylor
		if ((ysize/2-(s*approx)) >= 0 && (ysize/2-(s*approx)) < ysize) {
			gfx_point(xsize/2+s*x2+2, ysize/2-(s*approx));
		}
		x2 = x2 + 0.01;
	}
}


