// carnival.c
// 11/6/15
// Author: Taylor Rongaus
// This program will design a fun carnival ride with four different axes of motion

#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <stdlib.h>
#include "gfx2.h"


int main () {

	int xsize = 600;
	int ysize = 600;
	int dx = 0;
	char c;
	int radius = 50;
	int swingdirection1 = 1;
	int direction = 1;
	int ix = 0;
	double theta1 = 0.0;
	double theta2 = 0.0;
	double theta3 = 0.0; 
	
	

	gfx_open(xsize,ysize,"carnival.c");

	while (1){
		
		// clear window, initialize display
		gfx_clear();
		
		// draw horizontal line
		gfx_color(255,255,255);
		gfx_line(0,50,xsize,50);
		
		// draw first swinging line
		gfx_color(255,0,100);
		if (swingdirection1)
			theta1 = theta1 + 0.01;
		else
			theta1 = theta1 - 0.01;	
		if (theta1 >= 0.5)
			swingdirection1 = 0;
		else if (theta1 <= (-0.5))
			swingdirection1 = 1;
		gfx_line(ix+dx,50,ix+dx,200-50*sin(theta1));
		
		// draw second swinging line
		gfx_color(100,100,255);
/*		if (swingdirection1)
			gfx_line(ix+dx,200-50*sin(theta1),ix+dx+50*cos(theta1),350-50*sin(theta1));
		else if (!swingdirection1)
			gfx_line(ix+dx,200-50*sin(theta1),ix+dx-50*cos(theta1),350-50*sin(theta1));
*/		
		gfx_line(ix+dx,200-50*sin(theta1),ix+dx,350);	
		
		
		// draw large circle
		gfx_color(210,125,250);
		if (swingdirection1)
			gfx_circle(ix+dx+radius*cos(theta1),350+radius*sin(theta1),radius);
		else
			gfx_circle(ix+dx-radius*cos(theta1),350+radius*sin(theta1),radius);
		
		// draw smaller rotating circle
		gfx_color(255,255,255);
		gfx_line(ix+dx,350,ix+dx+(radius*cos(theta3)),350+(radius*sin(theta3)));
		gfx_circle(ix+dx+(radius*cos(theta3)),350+(radius*sin(theta3)),radius/5);
		
		
		// switch directions at end of display window
		if (direction)
			dx++;
		else 
			dx--;
		if ((ix+dx+radius) >= xsize){
			direction = 0;
		}
		else if ((ix+dx-radius) <= 0) {
			direction = 1;
		}
		
		theta3 = theta3 + 0.01;
		gfx_flush();
		usleep(8000);
	
		if (gfx_event_waiting()) {	
			c = gfx_wait();      				// wait for the user to hit a key
			if (c == 'q') break; 				// quit when a 'q' is pressed
		}
	
	}


}

