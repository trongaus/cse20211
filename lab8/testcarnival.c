// carnival.c
// 11/6/15
// Author: Taylor Rongaus
// This program will design a fun carnival ride with four different axes of motion

#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <stdlib.h>
#include "gfx2.h"

int motionone(int dx, int direction);


int main () {

	int xsize = 600;
	int ysize = 600;
	int dx = 0;
	char c;
	int radius = 50;
	int swingdirection1 = 1;
	int direction = 1;
	int ix = 0;
	double theta1 = 0.0;
	double theta2 = 0.0;
	double theta3 = 0.0; 
	
	

	gfx_open(xsize,ysize,"carnival.c");

	while (1){
		
		// clear window, initialize display
		gfx_clear();
		
		// draw horizontal line
		gfx_color(255,255,255);
		gfx_line(0,50,xsize,50);
		
		// draw first swinging line
		gfx_color(255,0,100);
		gfx_line(ix+motionone(dx,direction),50,ix+motionone(dx,direction),200);
		
		// draw second swinging line
		gfx_color(100,100,255);
		gfx_line(ix+motionone(dx,direction),200,ix+motionone(dx,direction),350);
		
		// draw large circle
		gfx_color(210,125,250);
		gfx_circle(ix+motionone(dx,direction),350,radius);
		
		// draw smaller rotating circle
		gfx_color(255,255,255);
		gfx_line(ix+motionone(dx,direction),350,ix+motionone(dx,direction)+(radius*cos(theta3)),350+(radius*sin(theta3)));
		gfx_circle(ix+motionone(dx,direction)+(radius*cos(theta3)),350+(radius*sin(theta3)),radius/5);
		
		
		// switch directions at end of display window
		if ((ix+dx+radius) >= xsize){
			direction = 0;
		}
		else if ((ix+dx-radius) <= 0) {
			direction = 1;
		}
		
		
		theta3 = theta3 + 0.01;
		gfx_flush();
		usleep(8000);
	
		if (gfx_event_waiting()) {	
			c = gfx_wait();      				// wait for the user to hit a key
			if (c == 'q') break; 				// quit when a 'q' is pressed
		}
	
	}


}


int motionone(int dx, int direction) {
	if (direction)
		dx++;
	else 
		dx--;
	return dx;

}

/*motiontwo() {



}

motionthree() {


}

motionfour() {


}
*/

