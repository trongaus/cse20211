// animate.c
// 11/1/15
// Author: Taylor Rongaus
// This program will create an animation of planetary motion that incorporates multiple shapes and colors, circular motion, and user control of the display.

#include <stdio.h>
#include "gfx2.h"
#include <math.h>

int main () {

	int xsize = 800;
	int ysize = 800;
	char c;
	double dtheta = 0.0;
	double dtheta2 = 0.0;
	int rc2 = -250;								// define radii of orbits
	int rc3 = -100;	
	int dt = 8000;
	int dx = 0;
	int dy = 0;
	int directionx = 1;
	int directiony = 1;
	int r1 = 80;
	int r2 = 40;
	int r3 = 20;
	
	gfx_open(xsize,ysize,"animate.c");
	
	while (1){
	
		//clear
		gfx_clear();
		
		// draw horizontal line
		gfx_color(0,255,0);
		gfx_line(0,50,xsize,50);
		
		//draw circle 1 and line
		gfx_color(255,255,255);
		gfx_line((xsize/2-r1/2)+dx,50,(xsize/2-r1/2)+dx,(ysize/2-r1/2)+dy);
		gfx_circle((xsize/2-r1/2)+dx,(ysize/2-r1/2)+dy,r1);
		
		//draw circle 2 and line
		gfx_color(0,0,255);
		gfx_line((xsize/2-r1/2)+dx,(ysize/2-r1/2)+dy,((xsize/2)-r2/2 +(rc2*cos(dtheta)))+dx,((ysize/2)-r2/2+(rc2*sin(dtheta)))+dy);
		gfx_circle(((xsize/2)-r2/2 +(rc2*cos(dtheta)))+dx,((ysize/2)-r2/2+(rc2*sin(dtheta)))+dy,r2);
		
		//draw circle 3 and line
		gfx_color(255,0,0);
		gfx_line(((xsize/2)-r2/2 +(rc2*cos(dtheta)))+dx,((ysize/2)-r2/2+(rc2*sin(dtheta)))+dy,((xsize/2)-r3/2 +(rc2*cos(dtheta)))+(rc3*cos(dtheta2))+dx,((ysize/2)-r3/2+(rc2*sin(dtheta)))+ (rc3*sin(dtheta2))+dy);
		gfx_circle(((xsize/2)-r3/2 +(rc2*cos(dtheta)))+(rc3*cos(dtheta2))+dx,((ysize/2)-r3/2+(rc2*sin(dtheta)))+ (rc3*sin(dtheta2))+dy,r3);
		
		
		//increment thetas
		dtheta = dtheta + (2*M_PI/360);
		dtheta2 = dtheta2 - (2*M_PI/360);
		
		// switch directions at end of display window
		if (directionx)
			dx++;
		else 
			dx--;
		if (directiony)	
			dy = dy+2;
		else
			dy = dy-2;
		
		if ((xsize/2+dx+r1) >= xsize){
			directionx = 0;
		}
		else if ((xsize/2+dx-r1) <= 0){
			directionx = 1;
		}
		if ((ysize/2+dy+r1) >= ysize){
			directiony = 0;
		}
		else if ((ysize/2+dy-r1) <= 50){
			directiony = 1;
		}
		
		gfx_flush;								// flush, sleep, & reset c
		usleep(dt);

		if (gfx_event_waiting()) {	
			c = gfx_wait();      				// wait for the user to hit a key
			if (c == 'q') break; 				// quit when a 'q' is pressed
		}
	
	
	}


}

