// prob3.c
// Author: Taylor Rongaus
// 9/20/15
// This program will act as a text menu driven basic calculator for the four simple arithmetic operations of addition, subtraction, multiplication, and division

#include <stdio.h>

int add (int a, int b);		// function prototypes
int subtract (int a, int b);
int multiply (int a, int b);
int divide (int a, int b);

int
main ()				// begin function main
{

  int num;
  int a;
  int b;
  int endprogram = 0;
  int answer;
  int checkchoice = 0;

  while (endprogram == 0)
    {
      while (checkchoice == 0)
	{
	  printf
	    ("\nWhat would you like to do? \n 1 for addition\n 2 for subtraction\n 3 for multiplication\n 4 for division\n 5 to quit\n");
	  printf ("Enter your choice: ");
	  scanf ("%d", &num);
	  if (num == 1 || num == 2 || num == 3 || num == 4 || num == 5)
	    {
	      checkchoice = 1;
	    }
	  else
	    {
	      printf ("Error. Please enter a valid choice.\n");
	    }
	  printf ("\n");
	}
      switch (num)
	{
	case 1:
	  printf ("Please enter a number: ");
	  scanf ("%d", &a);
	  printf ("\nPlease enter another number: ");
	  scanf ("%d", &b);
	  answer = add (a, b);
	  printf ("\nThe sum of %d and %d is %d.\n", a, b, answer);
	  checkchoice = 0;
	  break;
	case 2:
	  printf ("Please enter a number: ");
	  scanf ("%d", &a);
	  printf ("\nPlease enter another number: ");
	  scanf ("%d", &b);
	  answer = subtract (a, b);
	  printf ("\nThe difference of %d and %d is %d.\n", a, b, answer);
	  checkchoice = 0;
	  break;
	case 3:
	  printf ("Please enter a number: ");
	  scanf ("%d", &a);
	  printf ("\nPlease enter another number: ");
	  scanf ("%d", &b);
	  answer = multiply (a, b);
	  printf ("\nThe product of %d and %d is %d.\n", a, b, answer);
	  checkchoice = 0;
	  break;
	case 4:
	  printf ("Please enter a number: ");
	  scanf ("%d", &a);
	  printf ("\nPlease enter another number: ");
	  scanf ("%d", &b);
	  answer = divide (a, b);
	  if (answer == 0)
	    {
	      printf ("\nThe quotient of %d and %d is undefined.\n", a, b);
	    }
	  else
	    {
	      printf ("\nThe quotient of %d and %d is %d.\n", a, b, answer);
	    }
	  checkchoice = 0;
	  break;
	case 5:
	  printf ("Thank you!\n");
	  endprogram = 1;
	  break;
	}
    }

}				// end function main


int
add (int a, int b)		// begin function add
{
  int answer = a + b;
  return answer;
}				// end function add


int
subtract (int a, int b)		// begin function subtract
{
  int answer = a - b;
  return answer;
}				// end function subtract


int
multiply (int a, int b)		// begin function multiply
{
  int answer = a * b;
  return answer;
}				// end function multiply


int
divide (int a, int b)		// begin function divide
{
  if (b == 0)
    {
      printf ("Error: cannot divide by zero.");
      return 0;
    }
  else
    {
      int answer = a / b;
      return answer;
    }
}				// end function divide
