// prob2.c
// Author: Taylor Rongaus
// 9/20/15
// This program takes the x and y coordinates of a point in the Cartesian coordinate system and returns its corresponding polar coordinates and its quadrant/ location

#include <stdio.h>
#include <math.h>

#define PI 3.14159265

double changepolarr (int x, int y);	// function prototypes
double changepolartheta (int x, int y);
int determinelocation (int x, int y);

int
main ()				// begin function main
{

  int x;
  int y;
  double r;
  double theta;
  int location;

  printf ("Please enter the x coordinate of your point\n");
  scanf ("%d", &x);
  printf ("Please enter the y coordinate of your point\n");
  scanf ("%d", &y);

  r = changepolarr (x, y);
  theta = changepolartheta (x, y);

  printf
    ("The cooresponding polar coordinates of your point are (%.2lf,%.2lf).\n",
     r, theta);

  location = determinelocation (x, y);
  switch (location)
    {
    case 0:
      printf ("The coordinate is positioned at the origin.\n");
      break;
    case 1:
      printf ("The coordinate is in quadrant 1.\n");
      break;
    case 2:
      printf ("The coordinate is in quadrant 2.\n");
      break;
    case 3:
      printf ("The coordinate is in quadrant 3.\n");
      break;
    case 4:
      printf ("The coordinate is in quadrant 4.\n");
      break;
    case 5:
      printf ("The coordinate is on the x-axis.\n");
      break;
    case 6:
      printf ("The coordinate is on the y-axis.\n");
      break;
    }

  return 0;

}				// end function main



double
changepolarr (int x, int y)	// begin function changepolarr
{
  double r;
  r = sqrt ((x * x) + (y * y));
  return r;
}				// end function changepolarr


double
changepolartheta (int x, int y)	// begin function changepolartheta
{
  double theta;
  theta = atan2 (y, x) * 180 / PI;
  return theta;
}				// end function changepolartheta


int
determinelocation (int x, int y)	// begin function determinelocation
{
  int location;

  if ((x == 0) && (y == 0))
    {
      location = 0;		// 0 represents at the origin
    }
  if ((x > 0) && (y > 0))
    {
      location = 1;		// 1 represents in quadrant 1
    }
  if ((x < 0) && (y > 0))
    {
      location = 2;		// 2 represents in quadrant 2
    }
  if ((x < 0) && (y < 0))
    {
      location = 3;		// 3 represents in quadrant 3
    }
  if ((x > 0) && (y < 0))
    {
      location = 4;		// 4 represents in quadrant 4
    }
  if ((x != 0) && (y == 0))
    {
      location = 5;		// 5 represents on the x-axis
    }
  if ((x == 0) && (y != 0))
    {
      location = 6;		// 6 represents on the y-axis
    }

  return location;

}				// end function determinelocation
