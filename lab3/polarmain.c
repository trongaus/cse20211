#include "polarfn.h"


int
main ()				// begin function main
{

  int x;
  int y;
  double r;
  double theta;
  int location;

  printf ("Please enter the x coordinate of your point\n");
  scanf ("%d", &x);
  printf ("Please enter the y coordinate of your point\n");
  scanf ("%d", &y);

  r = changepolarr (x, y);
  theta = changepolartheta (x, y);

  printf
    ("The cooresponding polar coordinates of your point are (%.2lf,%.2lf).\n",
     r, theta);

  location = determinelocation (x, y);
  switch (location)
    {
    case 0:
      printf ("The coordinate is positioned at the origin.\n");
      break;
    case 1:
      printf ("The coordinate is in quadrant 1.\n");
      break;
    case 2:
      printf ("The coordinate is in quadrant 2.\n");
      break;
    case 3:
      printf ("The coordinate is in quadrant 3.\n");
      break;
    case 4:
      printf ("The coordinate is in quadrant 4.\n");
      break;
    case 5:
      printf ("The coordinate is on the x-axis.\n");
      break;
    case 6:
      printf ("The coordinate is on the y-axis.\n");
      break;
    }

  return 0;

}				// end function main
