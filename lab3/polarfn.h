// prob2.c
// Author: Taylor Rongaus
// 9/20/15
// This program takes the x and y coordinates of a point in the Cartesian coordinate system and returns its corresponding polar coordinates and its quadrant/ location

#include <stdio.h>
#include <math.h>

#define PI 3.14159265

double changepolarr (int x, int y);	// function prototypes
double changepolartheta (int x, int y);
int determinelocation (int x, int y);

