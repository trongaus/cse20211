// prob1.c
// Author: Taylor Rongaus
// 9/20/15
// This program takes two integers as its input and determines their GCD

#include <stdio.h>

int getgcd (int a, int b);			// function prototype

int main ()					// begin function main
{
  int a;
  int b;
  int gcd;

  printf ("Please enter an integer\n");
  scanf ("%d", &a);
  printf ("Please enter another integer\n");
  scanf ("%d", &b);

  gcd = getgcd(a,b);

  printf ("The GCD of these two numbers is %d\n", gcd);
  return 0;
}						// end function main

int getgcd(int a, int b)
{
	int gcd;
	int x;					// dummy swap variable
	int check = 0;				// while loop check
	int r;					// remainder

	if ((a == 0) || (b == 0)) { 		// check to see if either a & b are zero
     		 return 0;			// if so, GCD = 0
	}

	while (check == 0) {			// loop through the process until r = 0
		if (a < b) {			// check to see if a < b & if not, switch them
			x = a;
			a = b;
			b = x;
		}
		r = a % b; 			// determine remainder of a/b
	      	if (r == 0) {
			gcd = b;	 	// if remainder = 0, GCD = b
			check = 1;
			return gcd;
		}
		else {
			a = b;
			b = r;		
			}
	}
}						// end function getgcd

