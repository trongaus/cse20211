#include <iostream>
using namespace std;

void func1(int);
void func2(int *);
void func3(int &);

int main()
{
  int n;
  n = 5; func1(n); cout << n << endl;
  n = 5; func2(&n); cout << n << endl;
  n = 5; func3(n); cout << n << endl;
}

void func1 (int n)
{ n = 8; }

void func2 (int *p)
{ *p = 8; }

void func3 (int &n)
{ n = 8; }

