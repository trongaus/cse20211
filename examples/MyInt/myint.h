// class interface for MyInt
class MyInt {
  public:
    MyInt();
    MyInt(int);
    ~MyInt();
    void setvalue(int);
    int getvalue();
    void display();
  private:
    int value;
};

