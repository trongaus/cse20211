// driver to test the MyInt class
#include <iostream>
using namespace std;
#include "myint.h"

int main()
{
  MyInt a, b(5);

  a.display();
  b.display();
  cout << "a is " << a.getvalue() << endl;
  cout << "b is " << b.getvalue() << endl;
  b.setvalue(44);
  cout << "b is now " << b.getvalue() << endl;

  return 0;
}
