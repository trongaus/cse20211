// class implementation for MyInt
#include <iostream>
using namespace std;
#include "myint.h"

MyInt::MyInt()
{
 value = 0;
}

MyInt::MyInt(int a)
{ 
 value = a;
}

MyInt::~MyInt()
{ }

void MyInt::setvalue(int n)
{ 
  value = n; 
}

int MyInt::getvalue()
{
  return value; 
}

void MyInt::display()
{
  cout << "the value is " << value << endl; 
}

