// animate.c
// 11/1/15
// Author: Taylor Rongaus
// This program will create an animation of planetary motion that incorporates multiple shapes and colors, circular motion, and user control of the display.

#include <stdio.h>
#include "gfx2.h"
#include <math.h>

int main () {

	int xsize = 1000;
	int ysize = 800;
	char c;
	double dtheta = 0.0;
	double dtheta2 = 0.0;
	int rp1 = -150;								// define radii of orbits
	int rm1 = -50;								// note that planet and moon 1 have
	int rp2 = 300;								// negative radii so increment/
	int rm2 = 70;								// decrement functions are reversed
	int dt = 8000;
	
	gfx_open(xsize,ysize,"animate.c");
	
	while (1){
	
		switch (c) {
			case 'R':							// up arrow
				rp1--;							// increase radius of planet 1
				break;
			case 'Q':							// left arrow
				rm1++;							// decrease radius of moon 1
				break;
			case 'T':							// down arrow
				rp1++;							// decrease radius of planet 1
				break;
			case 'S':							// right arrow
				rm1--;							// increase radius of moon 1
				break;
			case 'w':							// secondary up arrow
				rp2++;							// increase radius of planet 2
				break;
			case 'a':							// secondary left arrow
				rm2--;							// decrease radius of moon 2
				break;
			case 's':							// secondary down arrow
				rp2--;							// decrease radius of planet 2
				break;
			case 'd':							// secondary right arrow
				rm2++;							// increase radius of moon 2
				break;
			case '-':							// minus key
				dt = dt + 100;
				break;
			case '=':							// plus (equals) key
				dt = dt - 100;
				break;		
		}
	
		//clear
		gfx_clear();
		
		//draw sun
		gfx_color(255,188,0);
		gfx_fill_arc(xsize/2-75,ysize/2-75,150,150,0,360);
		
		//draw planet 1
		gfx_color(0,0,255);
		gfx_fill_arc(((xsize/2)-20 +(rp1*cos(dtheta))),((ysize/2)-20+(rp1*sin(dtheta))),40,40,0,360);
		
		//draw moon 1
		gfx_color(200,200,200);
		gfx_fill_arc(((xsize/2)-10 +(rp1*cos(dtheta)))+(rm1*cos(dtheta2)),((ysize/2)-10+(rp1*sin(dtheta)))+ (rm1*sin(dtheta2)),20,20,0,360);
		
		//draw planet 2
		gfx_color(255,85,0);
		gfx_fill_arc(((xsize/2)-40 +(rp2*cos(dtheta))),((ysize/2)-40+(rp2*sin(dtheta))),80,80,0,360);
		
		//draw moon 2
		gfx_color(115,0,165);
		gfx_fill_arc(((xsize/2)-15 +(rp2*cos(dtheta)))+(rm2*cos(dtheta2)),((ysize/2)-15+(rp2*sin(dtheta)))+ (rm2*sin(dtheta2)),30,30,0,360);
		
		//increment thetas
		dtheta = dtheta + (2*M_PI/360);
		dtheta2 = dtheta2 - (2*M_PI/360);
		
		gfx_flush;								// flush, sleep, & reset c
		usleep(dt);
		c = '+';

		if (gfx_event_waiting()) {	
			c = gfx_wait();      				// wait for the user to hit a key
			if (c == 'q') break; 				// quit when a 'q' is pressed
		}
	
	
	}


}





