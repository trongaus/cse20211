// symbol.c
// 10/30/15
// Author: Taylor Rongaus
// This program will open up a window and waits for the user to press a mouse button or a key. On each key or button press, it will display a small symbol centered at the current mouse location.

#include <stdio.h>
#include "gfx.h"
#include <math.h>
// need -std=c99 to compile

void getPoly(int xpos, int ypos, int n);

int main()
{
	int xsize = 500;
	int ysize = 300;
	char c;
	int i,n;
	int xpos = 0;
	int ypos = 0;
 	
	gfx_open(xsize, ysize, "Symbol Program"); 	// Open a new window for drawing
	
	while(1) { 
		c = gfx_wait();							// Wait for user to press key/button
		xpos = gfx_xpos();						// Get x and y position of cursor
		ypos = gfx_ypos();
		switch (c){								// Switch for options 1,t, & c
			case 1:
				gfx_clear();					// Clear the window
				gfx_color(0,0,255);				// Set the drawing color
												// Draw a square centered around cursor
				getPoly(xpos,ypos,4);
				break;
			case 't':
				gfx_clear();					// Clear the window
				gfx_color(0,255,0);				// Set the drawing color
												// Draw a triangle centered around cursor
				getPoly(xpos,ypos,3);
;				break;
			case 'c':
				gfx_clear();					// Clear the window
				gfx_color(255,255,255);			// Set the drawing color
				gfx_circle(xpos,ypos,30);		// Draw a circle centered around cursor
				break;
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				gfx_clear();
				gfx_color(122,9,122);
				n = c - '0';
				getPoly(xpos,ypos,n);
				break;
		}	
		if(c == 'q') break;						// Quit if it is the letter q
	}

	return 0;

}

void getPoly(int xpos, int ypos, int n) {		// getPoly function
	int x1, x2, y1, y2, i;
	double theta;
	double newtheta = 0;
	int r = 30;
	theta = (2*acos(-1))/n;						// determine interior angle size
	x1 = xpos + r;								// start at "3'oclock"
	y1 = ypos;
	for (i=1;i<=n;i++) {
		newtheta = newtheta + theta;			// loop through number of sides
		x2 = xpos + (r*cos(newtheta));
		y2 = ypos + (r*sin(newtheta));
		gfx_line(x1,y1,x2,y2);
		x1 = x2;
		y1 = y2;
	}

}


