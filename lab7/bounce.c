// bounce.c
// 10/30/15
// Author: Taylor Rongaus
// This program creates an animation of a circle moving around the screen at a constant velocity. If the circle hits any of the four sides of the window, it will bounce off of the wall. If the user clicks on the screen with mouse button one, it will move the circle to the location of the click and give it a new random velocity.

#include <stdio.h>
#include "gfx.h"
#include <math.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>

int main () {

	int xsize = 500;
	int ysize = 300;
	char c;
	int radius = 40;
	int dx = 0, dy = 0;							// increments for animation
	int ix = 0, iy = 0;							// initial x and y position
	int rand_direction_x = 0;					// initialize random x, y directions
	int rand_direction_y = 0;
	int rand_pause_time = 0;					// initialize random pause time
	
	time_t t;
	srand(time(&t));							// initalize the random number generator

												// determine random initial x and y
												// within bounds of display window
	
	while ((ix < radius) || (ix > (xsize-radius))) {
		ix = rand()%xsize;						
	}	
	while ((iy < radius) || (iy > (ysize-radius))) {
		iy = rand()%ysize;
	}
	
	rand_direction_x = rand()%2;				// determine random x, y directions
	rand_direction_y = rand()%2;
	rand_pause_time = rand()%8000 + 3000;		// determine random pause time (ms)
	gfx_open(xsize,ysize,"bounce.c");			// open display window
	gfx_color(255,255,255);						// set color as while
	gfx_circle(ix,iy,radius);					// place circle randomly

	while(1) {
	
		if (c==1){								// if user clicks
		
			ix = gfx_xpos();					// new x,y = location of click
			iy = gfx_ypos();
		
			if ((ix+radius) > xsize)			// ensure user clicks aren't too close
				ix = xsize -radius;				// to walls
			else if ((ix-radius) < 0)
				ix = radius;
			
			if ((iy+radius) > ysize)
				iy = ysize -radius;
			else if ((iy-radius) < 0)
				iy = radius;

			dx = 0;								// reset dx,dy to 0
			dy = 0;
			rand_pause_time = rand()%8000 + 3000;	// new random pause time
			c = '+';							// assure only enter "if" upon click
		}
	
		gfx_clear();
		gfx_circle(ix+dx,iy+dy,radius);
		gfx_flush;
		
		if (rand_direction_x)					// if random x direction = 1
			dx++;								// proceed to the right
		else									// if 0, proceed left
			dx--;
		if (rand_direction_y)					// if random x direction = 1
			dy++;								// proceed downwards
		else									// if 0, proceed upwards
			dy--;
		
		usleep(rand_pause_time);				// pause for random amount of time
		
												// switch x,y directions if contact
												// with wall is made
		
		if (((ix+dx+radius) > xsize) || ((ix+dx-radius) < 0)) {
			if (rand_direction_x)
				rand_direction_x = 0;
			else
				rand_direction_x = 1;
		}
		
		if (((iy+dy+radius) > ysize) || ((iy+dy-radius) < 0)) {
			if (rand_direction_y)
				rand_direction_y = 0;
			else
				rand_direction_y = 1;
		}
		
		if (gfx_event_waiting()) {	
			c = gfx_wait();      				// wait for the user to hit a key
			if (c == 'q') break; 				// quit when a 'q' is pressed
		}
		
		
		
	}



}
