// testing.c

#include <stdio.h>
#include "gfx.h"
#include <math.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>

int main () {

	int xsize = 500;
	int ysize = 300;
	char c;
	int radius = 40;
	int dx = 0, dy = 0;							// increments for animation
	int ix = 0, iy = 0;							// initial x and y position
	int rand_direction_x = 0;					// initialize random x, y directions
	int rand_direction_y = 0;
	int rand_pause_time = 0;					// initialize random pause time
	
	time_t t;
	srand(time(&t));							// initalize the random number generator

												// determine random initial x and y
												// within bounds of display window
	
	while ((ix < radius) || (ix > (xsize-radius))) {
		ix = rand()%xsize;						
	}	
	while ((iy < radius) || (iy > (ysize-radius))) {
		iy = rand()%ysize;
	}
	
	rand_direction_x = rand()%2;				// determine random x, y directions
	rand_direction_y = rand()%2;
	rand_pause_time = rand()%8000 + 3000;		// determine random pause time (ms)

//	printf("%d %d\n",ix,iy);	
//	printf("%d\n",rand_pause_time);						
//	printf("%d %d\n", rand_direction_x, rand_direction_y);
	
	gfx_open(xsize,ysize,"testing");			// open display window
	gfx_color(255,255,255);						// set color as while
	gfx_circle(ix,iy,radius);					// place circle randomly

	while(1) {
	
		if (c==1){								// if user clicks
			ix = gfx_xpos();					// new x,y = location of click
			iy = gfx_ypos();
			dx = 0;								// reset dx,dy to 0
			dy = 0;
			rand_pause_time = rand()%8000 + 3000;	// new random pause time
			c = '+';							// assure only enter "if" upon click
		}
	
		gfx_clear();
		gfx_circle(ix+dx,iy+dy,radius);
		gfx_flush;
		
		if (rand_direction_x)					// if random x direction = 1
			dx++;								// proceed to the right
		else									// if 0, proceed left
			dx--;
		if (rand_direction_y)					// if random x direction = 1
			dy++;								// proceed downwards
		else									// if 0, proceed upwards
			dy--;
		
		usleep(rand_pause_time);				// pause for random amount of time
		
												// switch x,y directions is contact
												// with wall is made
		
		if (((ix+dx+radius) > xsize) || ((ix+dx-radius) < 0)) {
			if (rand_direction_x)
				rand_direction_x = 0;
			else
				rand_direction_x = 1;
		}
		
		if (((iy+dy+radius) > ysize) || ((iy+dy-radius) < 0)) {
			if (rand_direction_y)
				rand_direction_y = 0;
			else
				rand_direction_y = 1;
		}
		
		if (gfx_event_waiting()) {	
			c = gfx_wait();      				// wait for the user to hit a key
			if (c == 'q') break; 				// quit when a 'q' is pressed
		}
		
		
		
	}



}
