// animate.c
// 11/1/15
// Author: Taylor Rongaus
// This program will create a little animation that incorporates multiple shapes and colors, circular motion, and user control of the display in some way.

#include <stdio.h>
#include "gfx2.h"
#include <math.h>

int main () {

	int xsize = 1000;
	int ysize = 600;
	int xpos = 0;
	int ypos = 0;
	char c;
	int dtheta = 0;
	int dt = 0;
	
	gfx_open(xsize,ysize,"animate.c");


	while (1) {
		
		// initialize display
		gfx_color(130,100,6);
		gfx_line((xsize/2),(ysize/2)+50,(xsize/2),ysize);
		gfx_line((xsize/2)-1,(ysize/2)+50,(xsize/2)-1,ysize);
		gfx_line((xsize/2)+1,(ysize/2)+50,(xsize/2)+1,ysize);
		gfx_line((xsize/2)-130,(ysize/2)+100,(xsize/2),ysize-50);
		gfx_line((xsize/2)+130,(ysize/2)+100,(xsize/2),ysize-50);
		gfx_color(212,200,135);
		gfx_fill_arc((xsize/2)-75,(ysize/2),150,150,0,360);
		gfx_color(255,255,255);
		gfx_fill_arc((xsize/2)-45,(ysize/2)+45,35,35,0,180);
		gfx_fill_arc((xsize/2)+12,(ysize/2)+45,35,35,0,180);
		gfx_circle((xsize/2)-130,(ysize/2)+100,10);
		gfx_color(108,154,246);
//		gfx_arc((xsize/2)-130,(ysize/2)+50,260,100,0,180);
//		gfx_arc((xsize/2)-130,(ysize/2)-100,260,400,0,180);
		gfx_circle(xsize/2,(ysize/2)+100,130);
		gfx_fill_arc((xsize/2)-36,(ysize/2)+47,15,15,0,360);
		gfx_fill_arc((xsize/2)+23,(ysize/2)+47,15,15,0,360);
		gfx_color(0,0,0);
		gfx_fill_arc((xsize/2)-31,(ysize/2)+50,5,5,0,360);
		gfx_fill_arc((xsize/2)+27,(ysize/2)+50,5,5,0,360);
		gfx_color(130,100,6);
		gfx_line((xsize/2),(ysize)/2+75,(xsize/2)+10,(ysize/2)+100);
		gfx_line((xsize/2)-5,(ysize/2)+100,(xsize/2)+10,(ysize/2)+100);
		gfx_color(220,60,60);
		gfx_fill_arc((xsize/2)-25,(ysize/2)+100,50,28,0,-180);
		gfx_color(53,176,94);
//		gfx_fill_arc((xsize/2)-80,(ysize/2)-40,100,100,10,210);
//		gfx_fill_arc((xsize/2)-78,(ysize/2),150,50,20,200);
		
		
		gfx_color(255,255,255);
		gfx_clear();
//		xpos = gfx_xpos();
//		ypos = gfx_ypos();
		gfx_circle(((xsize/2)+(130*cos(dtheta))),((ysize/2)+100+(130*sin(dtheta))),10);
		dtheta++;
		gfx_flush;
		usleep(10000);
		
		
		if (gfx_event_waiting()) {	
			c = gfx_wait();      				// wait for the user to hit a key
			if (c == 'q') break; 				// quit when a 'q' is pressed
		}
	}


}


