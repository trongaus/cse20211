// crossword.c
// Authors: Madeline Gleason and Taylor Rongaus
// Date: 12/6/15
// This program, when given a list of words, creates a blank crossword anagram puzzle that can be played with paper and pencil. The program will also provide a solution for the player who gets stuck.

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define MAXWORDLENGTH 15
#define MAXNUMWORDS 20

void readWords(char words[][MAXNUMWORDS], int *count, int *lengthswitch);
void emptyBoard(char board[][MAXWORDLENGTH]);
void emptySolution(char solution[][MAXWORDLENGTH]);
void longestWord(int *count, char words[][MAXNUMWORDS], int *longest);

int main ()
{

char words[MAXWORDLENGTH][MAXNUMWORDS];		// store words in a 2D array
char board[MAXWORDLENGTH][MAXWORDLENGTH];	// create 2D array to store empty dots
char solution[MAXWORDLENGTH][MAXWORDLENGTH];	// create 2D array to store empty hashtags for solution
int count = 0;					// counter for number of words entered if ended by a '.'
int longest = 0;				// stores length of longest word entered
int lengthswitch = 0;

	readWords(words, &count, &lengthswitch);
	if (lengthswitch == 0) {
		emptyBoard(board);
		emptySolution(solution);
		longestWord(&count, words, &longest);
	}
}

void readWords(char words[][MAXNUMWORDS], int *count, int *lengthswitch)
{
	int i, j;
	printf("Enter a list of up to 20 words, up to 15 letters each.\n");
	printf("Enter a '.' character when finished. \n");
		for (i = 0; i < MAXNUMWORDS; i++){
			scanf ("%s", words[i]);
			(*count)++;						// count all entered rows including terminating '.'
			if (strlen(words[i]) > 15){		// check entered words are less than 15 characters
				printf("Error: words cannot exceed 15 characters.\n");	
				*lengthswitch = 1;
				return;
			}
			for (j = 0; j < MAXWORDLENGTH; j++){			// check entered words contain alphabetical characters
				if (isalpha(words[i][j]) == 1){				
					printf("Words can only contain alphabetical characters.\n");
					return;
				}
			}
			// ^^^^^^ trying to figure out how to check and what to display if entered word is not a letter
			if (words[i][0] == '.'){ 	// stop reading in words if user enters a period
				break;
			}	
		}
	printf("Count is: %d\n", *count);
}

void emptyBoard(char board[][MAXWORDLENGTH])
{ 
int j, k;						// declare counters
	for (j = 0; j <= MAXWORDLENGTH; j++){
		printf ("\n");				// move to new row
		for (k = 0; k <= MAXWORDLENGTH; k++){
			board[j][k] = '#';
			printf ("%c", board[j][k]);	// print a '.' across one row
		}
	}
	printf ("\n");
}

void emptySolution(char solution[][MAXWORDLENGTH])
{ 
	int m, n;					// declare counters
	for (m = 0; m <= MAXWORDLENGTH; m++){
		printf ("\n");				// move to new row
		for (n = 0; n <= MAXWORDLENGTH; n++){
			solution[m][n] = '.';
			printf ("%c", solution[m][n]);	// print a '#' across one row
		}
	}
	printf ("\n");
}

void longestWord(int *count, char words[][MAXNUMWORDS], int *longest)
{
	int i;							// counter
	int maxwordlength = 0;					// stores the length of the longest word
	int lengths[MAXNUMWORDS];				// array with lengths of each word
	for (i = 0; i < (*count); i++){			
		lengths[i] = strlen(words[i]);		
		printf("Length of row %d is: %d\n", i, lengths[i]);	// print the length of each word in each row
	}	// do not need ^^^^^^^^ using for debugging purposes
	for (i = 0; i < (*count); i++) {
		if(lengths[i] >= maxwordlength) {
			maxwordlength = lengths[i];		// find longest word
			*longest = i;				// return row of longest word 	
		}	
	}
	printf("The longest word is in row: %d\n", *longest);	// print the row with the longest word
	printf("%s\n", words[*longest]);			// print the longest word 
}
