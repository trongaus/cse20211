// prog2.c
// Author: Taylor Rongaus
// 9/6/15

// Football points addition program.
#include <stdio.h>

// function main begins program execution
int main(void)
{
	int integer1; // first number to be entered by user
	int integer2; // second number to be entered by user
	int integer3; // third number to be entered by user
	int integer4; // fourth number to be entered by user
	int sum; // variable in which sum will be stored

	printf("Enter number of touchdowns scored\n"); // prompt
	scanf("%d", &integer1); //read first integer

	printf("Enter number of extra points scored\n"); // prompt
	scanf("%d", &integer2); //read second integer

	printf("Enter number of field goals scored\n"); // prompt
	scanf("%d", &integer3); //read third integer

	printf("Enter number of safeties scored\n"); // prompt
	scanf("%d", &integer4); //read fourth integer

	sum = 6*integer1 + integer2 + 3*integer3 + 2*integer4; // point sum

	printf("The Irish scored %d points.\n",sum); // print sum
} // end function main
