// prog3.c
// Author: Taylor Rongaus
// 9/6/15

// Pythagoream theorem program
#include <stdio.h>
#include <math.h>

// function main begins program execution
int main(void)

{
	double a; // first side length to be entered by user
	double b; // second side length to be entered by user
	double c; // hypotenuse value to be returned

	printf("This program computes the length of the hypotenuse of a right triangle using Pythagorean's Theorem.\n");
	printf("Enter the length of side a\n");
		scanf("%lf", &a);

	if (a<=0)
                {
                printf("a must be a positive real number\n");
                return 0 ;
                }

	printf("Enter the length of side b\n");
		scanf("%lf", &b);
	
	if (b<=0)
		{
		printf("b must be a positive real number\n");
		return 0;
		}

	c = sqrt((a*a)+(b*b));
	printf("The length of your hypotenuse is %.2lf.\n",c);

}

