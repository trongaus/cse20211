// prog1.c
// Author: Taylor Rongaus
// 9/6/15

// Fig. 2.1: fig02_01.c
// A first program in C.
#include <stdio.h>

// function main begins program execution
int main(void)
{
printf("Welcome to C!\n");
} // end function main
